import IImageItem from "@/interface/IImageItem";

export default interface ILink extends IImageItem {
    link: number;
}
